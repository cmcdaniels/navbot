#include <Servo.h>
#include <NewPing.h>
#include <math.h>
const int PING_PIN = 22;
const int MAX_DISTANCE = 500;
const int motorA1 = 8;
const int motorA2 = 9;
const int motorB1 = 11;
const int motorB2 = 10;
const int ratio = 254/255;
const int speed = 255;


int count = 0;
double veerDir = 0;
NewPing sonar(PING_PIN, PING_PIN, MAX_DISTANCE);

Servo servo;

void setup(){
  pinMode(motorA1, OUTPUT);
  pinMode(motorA2, OUTPUT);
  pinMode(motorB1, OUTPUT);
  pinMode(motorB2, OUTPUT);
  
  
  Serial.begin(115200);
  servo.attach(2);

}

void loop(){
  
  scan();
 
 
  

}

int checkObstacle(){
 
  int ping = sonar.ping_cm();
  Serial.print("Ping: ");
  Serial.print(ping);
  Serial.println(" cm");
  
  
  if(ping <= 30 && ping != 0){
    count++;
    
    if(count > 5){
      veer(ping);
      count = 0;
    }
  }
  
  return ping;
 
}

void veer(int cm){
    
  while(cm <= 30){
  
      if(veerDir < 90){
        digitalWrite(motorA1, LOW);
        analogWrite(motorA2, 255);
        digitalWrite(motorB1, HIGH);
        analogWrite(motorB2, 255);
        delay(200);
        cm = checkObstacle();
      }else{
        digitalWrite(motorA1, HIGH);
        analogWrite(motorA2, 255);
        digitalWrite(motorB1, LOW);
        analogWrite(motorB2, 255);
        delay(200);
        cm = checkObstacle(); 
      }
    } 
    
    servo.write(90);
  

}
void drive(){
   digitalWrite(motorA1, HIGH);
   analogWrite(motorA2, speed);
  
  
  digitalWrite(motorB1, HIGH);
  analogWrite(motorB2, speed);
}
void scan(){
  double x = 0;
  while (true){
   
    veerDir = fabs(95*sin(x)) + 45;
    checkObstacle();
    servo.write(fabs(veerDir));
    delay(50);
    x += .1;
    drive();
}
} 
 
 









